# contect: Results

This repository contains the result files of my thesis project, which explores the possibilities of detecting condense trails on MODIS satellite spectral imaginary.

### Directories

A folder's name consists of the following attributes:
```
<date>-<time>_<method>_<mask>_<number of files>
```

- **date**: date in `ddmmyy` format
- **time**: time in `hhmm` format (UTC)
- **method**: Hough transformation method used (standard, probabilistic)
  - standard: Standard Hough Transformation (see [scikit-image](https://scikit-image.org/docs/dev/auto_examples/edges/plot_line_hough_transform.html))
  - probabilistic: Progressive Probabilistic Hough Transformation (see [scikit-image](https://scikit-image.org/docs/dev/api/skimage.transform.html#skimage.transform.probabilistic_hough_line))
- **mask**: self-defined mask
  - a: very sensitive
  - b: less sensitive
  - c: least sensitive

### Files

#### Meta Information
Each folder contains 3 files with meta information:
- **config.json**: all parameters used in this run
- **results.csv**: key numbers (e.g. number of flights, accuracy, ...)
- **processed_files.csv**: list of used files

#### Images
There are 2 files for each satellite image. One showing the calculated brightness temperature difference and the second the detected condense trails as a red mask.

